const images = [
    'img/chanel.jpg',
    'img/vogue.jpg',
    'img/vogue2.jpg',
    'img/vogue3.jpg',
];

let currentIndex = 0;

const container = document.getElementById('slideshow-container');

images.forEach((imageSrc, index) => {
    const img = document.createElement('img');
    img.src = imageSrc;
    img.classList.add('slideshow-image');
    if (index === 0) {
        img.classList.add('active');
    }
    container.appendChild(img);
});

const prevButton = document.createElement('button');
prevButton.id = 'prev';
prevButton.className = 'nav-button';
prevButton.innerText = 'Föregående';
container.appendChild(prevButton);

const nextButton = document.createElement('button');
nextButton.id = 'next';
nextButton.className = 'nav-button';
nextButton.innerText = 'Nästa';
container.appendChild(nextButton);

const slideshowImages = document.querySelectorAll('.slideshow-image');

function showImage(index) {
    slideshowImages.forEach((img, i) => {
        img.classList.toggle('active', i === index);
        img.style.display = i === index ? 'block' : 'none';
    });
}

function showPrevImage() {
    currentIndex = (currentIndex > 0) ? currentIndex - 1 : images.length - 1;
    showImage(currentIndex);
}

function showNextImage() {
    currentIndex = (currentIndex < images.length - 1) ? currentIndex + 1 : 0;
    showImage(currentIndex);
}

prevButton.addEventListener('click', showPrevImage);
nextButton.addEventListener('click', showNextImage);













